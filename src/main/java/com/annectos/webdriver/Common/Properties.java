package com.annectos.webdriver.Common;

/**
 * Created with IntelliJ IDEA.
 * User: QA-002
 * Date: 1/13/14
 * Time: 3:35 PM
 * To change this template use File | Settings | File Templates.
 */


import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class Properties extends Testbase{

    private static long beginTime;
    private static long endTime;

    public static String userNamespan;
    public static String passwordspan;
    public static String userNamewipro;
    public static String passwordwipro;
    public static String userNamexolo;
    public static String passwordxolo;
    public static String wiproLogo;
    public static String userNamegold;
    public static String passwordgold;
    public static String Level1Menu;
    public static String Level0Menu;


    public static String userNameseep;
    public static String passwordseep;
    public static String firstnameSeep;
    public static String lastnameSeep;
    public static String MobileNoseep;
    public static String DOBSeep;
    public static String CFDSeep;
    public static String Educationseep;
    public static  String InvoiceNumberRishtey;
    public static  String InvoiceAmountRishtey;
    public static  String InvoiceDateSeep;
    public static  String InvoiceDistributorRishtey;
    public static String StreetAddress ;
    public static String Landmark ;
    public static String City ;
    public static String State;
    public static String Pincode;
    public static String PhoneNumber;
    public static String SeepAdmin;
    public static String SeepAdminPwd;
    public static String SeepSchAdmin;
    public static String SeepSchAdminPwd;

    public static String userNamerishtey;
    public static String passwordrishtey;
    public static String firstnamerishtey;
    public static String lastnamerishtey;
    public static String MobileNorishtey;
    public static String DOBrishtey;
    public static String CFDrishtey;
    public static String Educationrishtey;

    public static String userNameSamplePromo;
    public static String PasswordSamplePromo;
    public static String firstnamepromo;
    public static String lastnamepromo;
    public static String companypromo;
    public static String designationpromo;
    public static String eamilidpromo;
    public static String phonepromo;
    public static String addressLine1promo;
    public static String addressLine2promo;
    public static String zipcodepromo;

    public static Random rand = new Random();
    static String amount = String.valueOf(rand.nextInt(500000) + 4000);
    static String invnumber= String.valueOf(rand.nextInt(100)+9999);
    public static String RishteyAdmin;
    public static String RishteyAdminPwd;
    public static String RishteySchAdmin;
    public static String RishteySchAdminPwd;
    public   static String InvoiceDateRishtey;
    public static String InvoiceDateRishteyNov;

    public static String generate_random_date_java() {

        beginTime = Timestamp.valueOf("2014-06-01 00:00:00").getTime();
        endTime = Timestamp.valueOf("2014-12-31 00:58:00").getTime();

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd/MM/yyyy");
        long diff = endTime - beginTime + 1;
        long getRandomTimeBetweenTwoDates= beginTime + (long) (Math.random() * diff);

        Date randomDate = new Date(getRandomTimeBetweenTwoDates);

        System.out.println(randomDate.getMonth());
        System.out.println(dateFormat.format(randomDate));
        return (dateFormat.format(randomDate));
    }

    public static String generate_random_date_java_before_november() {

        beginTime = Timestamp.valueOf("2014-06-01 00:00:00").getTime();
        endTime = Timestamp.valueOf("2014-11-01 00:58:00").getTime();

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd/MM/yyyy");
        long diff = endTime - beginTime + 1;
        long getRandomTimeBetweenTwoDates= beginTime + (long) (Math.random() * diff);

        Date randomDate = new Date(getRandomTimeBetweenTwoDates);

        System.out.println(randomDate.getMonth());
        System.out.println(dateFormat.format(randomDate));
        return (dateFormat.format(randomDate));
    }


    public static void setVariables() {

        userNamespan = XMLFunctions.getXMLConfiguration(CONFIG_FILE, "Span.email");
        passwordspan = XMLFunctions.getXMLConfiguration(CONFIG_FILE,"Span.password");
        userNamewipro = XMLFunctions.getXMLConfiguration(CONFIG_FILE,"Wipro.email");
        passwordwipro= XMLFunctions.getXMLConfiguration(CONFIG_FILE,"Wipro.password");
        userNamexolo = XMLFunctions.getXMLConfiguration(CONFIG_FILE,"xolo.email");
        passwordxolo= XMLFunctions.getXMLConfiguration(CONFIG_FILE,"xolo.password");
        wiproLogo= XMLFunctions.getXMLConfiguration(CONFIG_FILE,"Wipro.LogoLocation");
        Level1Menu=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"Menu.Level1");
        Level0Menu=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"Menu.Level0");
        passwordgold=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"gold.password");
        userNamegold =XMLFunctions.getXMLConfiguration(CONFIG_FILE,"gold.email");

        //Seep
        userNameseep=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"seep.email");
        passwordseep=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"seep.password");
        firstnameSeep=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"seep.firstname");
        lastnameSeep=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"seep.lastname");
        DOBSeep=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"seep.DOB");
        CFDSeep=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"seep.CFD");
        Educationseep=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"seep.Education");
        MobileNoseep=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"seep.mobileNo");

        //Rishtey
        userNamerishtey=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"rishtey.email");
        passwordrishtey=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"rishtey.password");
        firstnamerishtey=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"rishtey.firstname");
        lastnamerishtey=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"rishtey.lastname");
        DOBrishtey=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"rishtey.DOB");
        CFDrishtey=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"rishtey.CFD");
        Educationrishtey=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"rishtey.Education");
        MobileNorishtey=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"rishtey.mobileNo");

        //SamplePromo

         userNameSamplePromo=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"samplepromo.firstname");
         PasswordSamplePromo=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"samplepromo.lastname");
         firstnamepromo=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"samplepromo.firstname");
         lastnamepromo =XMLFunctions.getXMLConfiguration(CONFIG_FILE,"samplepromo.lastname");
         companypromo=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"samplepromo.Company");
         designationpromo=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"samplepromo.Designation");
         eamilidpromo=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"samplepromo.Email");
         phonepromo=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"samplepromo.phone");
         addressLine1promo=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"samplepromo.addressLine1");
         addressLine2promo=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"samplepromo.addressLine2");
         zipcodepromo=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"samplepromo.Zipcode");


        //Profile Page Seep Address

        StreetAddress=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"rishtey.StreetAddress") ;
        Landmark=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"rishtey.Landmark") ;
        City=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"rishtey.City") ;
        State=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"rishtey.State");
        Pincode=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"rishtey.Pincode");
        PhoneNumber=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"rishtey.PhoneNumber");
        RishteyAdmin=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"rishtey.admin");
        RishteyAdminPwd=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"rishtey.adminPwd");
        RishteySchAdmin=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"rishtey.SchAdmin");
        RishteySchAdminPwd=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"rishtey.PwdSchAdmin");

        InvoiceNumberRishtey =XMLFunctions.getXMLConfiguration(CONFIG_FILE,"rishtey.invoicenumber")+invnumber;
        InvoiceAmountRishtey =amount;
        // InvoiceDateSeep=XMLFunctions.getXMLConfiguration(CONFIG_FILE,"seep.invoicedate");
        InvoiceDateRishtey=generate_random_date_java();
        InvoiceDateRishteyNov=generate_random_date_java_before_november();
        InvoiceDistributorRishtey =XMLFunctions.getXMLConfiguration(CONFIG_FILE,"rishtey.invoiceDistri");

    }






}

