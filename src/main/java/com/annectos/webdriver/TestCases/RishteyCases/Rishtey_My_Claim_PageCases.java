package com.annectos.webdriver.TestCases.RishteyCases;

import com.annectos.webdriver.Common.Staticprovider;
import com.annectos.webdriver.Common.Testbase;

import com.annectos.webdriver.PageObjects.RishteyClaim.Rishtey_Claim_Entry_Page_Objects;
import com.annectos.webdriver.PageObjects.RishteyClaim.Rishtey_Home_PageObjects;
import com.annectos.webdriver.PageObjects.RishteyClaim.Rishtey_Login_PageObjects;
import com.annectos.webdriver.PageObjects.RishteyClaim.Rishtey_My_Claims_PageObjects;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by praveen on 7/9/2014.
 */
public class Rishtey_My_Claim_PageCases extends Testbase{


    //Adds A Claim in Claim Entry Page and Checks whether the Claim is Showing in my claims page and The Status is Pending
        @Test(dataProviderClass = Staticprovider.class,dataProvider="RishteyData")
        public void it_should_display_the_claim_added(String Admin, String AdminPwd, String SchAdmin, String schPassword, String email, String Password, String invoiceNumber, String amount, String date, String distrib, String Datenov) throws InterruptedException, ParseException {

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date dte = dateFormat.parse(date);
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MMMMMMM");
            String finaldate=dateFormat1.format(dte);

            if (threadDriver != null) {
                // findRemoteip(threadDriver.get());
            }
            Rishtey_Home_PageObjects rishteyHomePageObjects = PageFactory.initElements(getDriver(), Rishtey_Home_PageObjects.class);
            Rishtey_Login_PageObjects rishteyLoginPageObjects = PageFactory.initElements(getDriver(), Rishtey_Login_PageObjects.class);
            rishteyLoginPageObjects.Navigate_to_baseURL();
            rishteyLoginPageObjects.Logout();
            rishteyLoginPageObjects.Login_with_both_Email_and_Password(email, Password);
            rishteyLoginPageObjects.elmCLAIMENTRYMenu.click();
            Rishtey_Claim_Entry_Page_Objects claim_entry_page_objects = PageFactory.initElements(getDriver(), Rishtey_Claim_Entry_Page_Objects.class);
            claim_entry_page_objects.Entering_valid_claim_and_save(invoiceNumber, amount, finaldate, distrib);
            rishteyLoginPageObjects.elmMYCLAIMSMenu.click();
            Rishtey_My_Claims_PageObjects Rishtey_My_Claims_PageObjects = PageFactory.initElements(getDriver(), Rishtey_My_Claims_PageObjects.class);
            Rishtey_My_Claims_PageObjects.check_entered_claim_shows_and_The_Status(invoiceNumber, "pending");
            rishteyLoginPageObjects.Logout();

        }

    @Test(dataProviderClass = Staticprovider.class,dataProvider="RishteyData")
    public void Check_all_Labels(String Admin, String AdminPwd, String SchAdmin, String schPassword, String email, String Password, String invoiceNumber, String amount, String date, String distrib, String Datenov) throws InterruptedException, ParseException {
        Rishtey_Home_PageObjects rishteyHomePageObjects = PageFactory.initElements(getDriver(), Rishtey_Home_PageObjects.class);
        Rishtey_Login_PageObjects rishteyLoginPageObjects = PageFactory.initElements(getDriver(), Rishtey_Login_PageObjects.class);
        rishteyLoginPageObjects.Navigate_to_baseURL();
        rishteyLoginPageObjects.Logout();
        rishteyLoginPageObjects.Login_with_both_Email_and_Password(email, Password);
        rishteyLoginPageObjects.elmMYCLAIMSMenu.click();
        Rishtey_My_Claims_PageObjects Rishtey_My_Claims_PageObjects = PageFactory.initElements(getDriver(), Rishtey_My_Claims_PageObjects.class);
        Rishtey_My_Claims_PageObjects.Checkall_labels( );
        rishteyLoginPageObjects.Logout();

    }




        }

