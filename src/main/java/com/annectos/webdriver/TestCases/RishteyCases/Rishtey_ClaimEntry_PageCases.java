package com.annectos.webdriver.TestCases.RishteyCases;

import com.annectos.webdriver.Common.Staticprovider;
import com.annectos.webdriver.Common.Testbase;
import com.annectos.webdriver.PageObjects.RishteyClaim.Rishtey_Claim_Entry_Page_Objects;
import com.annectos.webdriver.PageObjects.RishteyClaim.Rishtey_Home_PageObjects;
import com.annectos.webdriver.PageObjects.RishteyClaim.Rishtey_Login_PageObjects;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by praveen on 11/12/2014.
 */
public class Rishtey_ClaimEntry_PageCases extends Testbase {



    //Verifies appropriate alert is given when mandatory fields are left blank while adding a new Claim
    @Test(dataProviderClass = Staticprovider.class,dataProvider="RishteyData")
    public void Claim_entry_mandatory_field_verification(String Admin, String AdminPwd,String SchAdmin,String schPassword,String email, String Password,String invoiceNumber,String amount,String  date,String distrib,String Datenov) throws InterruptedException, ParseException {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date dte = dateFormat.parse(date);
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MMMMMMM");
        String finaldate=dateFormat1.format(dte);

        Rishtey_Home_PageObjects rishteyHomePageObjects= PageFactory.initElements(getDriver(), Rishtey_Home_PageObjects.class);
        Rishtey_Login_PageObjects rishteyLoginPageObjects=PageFactory.initElements(getDriver(),Rishtey_Login_PageObjects.class);
        rishteyLoginPageObjects.Navigate_to_baseURL();
        rishteyLoginPageObjects.Logout();
        rishteyLoginPageObjects.Login_with_both_Email_and_Password(email, Password);
        rishteyHomePageObjects.Goto_Claim_entry_page();
        Rishtey_Claim_Entry_Page_Objects rishteyClaimEntryPageObjects = PageFactory.initElements(getDriver(), Rishtey_Claim_Entry_Page_Objects.class);
        rishteyClaimEntryPageObjects.verify_alert_When_invoice_number_left_blank(invoiceNumber, amount, finaldate, distrib);
        rishteyHomePageObjects.Gotto_Conact_page();
        Thread.sleep(1000);
        rishteyHomePageObjects.Goto_Claim_entry_page();

        rishteyClaimEntryPageObjects.verify_alert_When_invoice_date_left_blank(invoiceNumber, amount, finaldate, distrib);
        rishteyHomePageObjects.Gotto_Conact_page();
        Thread.sleep(1000);

        rishteyHomePageObjects.Goto_Claim_entry_page();
        rishteyClaimEntryPageObjects.verify_alert_When_invoice_amount_left_blank(invoiceNumber, amount, finaldate, distrib);
        rishteyHomePageObjects.Gotto_Conact_page();
        Thread.sleep(1000);

        rishteyHomePageObjects.Goto_Claim_entry_page();
        rishteyClaimEntryPageObjects.verify_alert_When_Distributor_left_blank(invoiceNumber, amount, finaldate, distrib);
    }


    //Verifies all Elements in Claim Entry page are present
    @Test(dataProviderClass = Staticprovider.class,dataProvider = "RishteyData")

    public void Claim_Page_Element_check(String Admin, String AdminPwd,String SchAdmin,String schPassword,
                                         String email, String Password,String invoiceNumber,String amount,
                                         String  date,String distrib,String Datenov) throws InterruptedException
    {
        Rishtey_Home_PageObjects Rishtey_Home_PageObjects = PageFactory.initElements(getDriver(), Rishtey_Home_PageObjects.class);
        Rishtey_Login_PageObjects rishteyLoginPageObjects=PageFactory.initElements(getDriver(),Rishtey_Login_PageObjects.class);
        rishteyLoginPageObjects.Navigate_to_baseURL();
        rishteyLoginPageObjects.Logout();

        rishteyLoginPageObjects.Login_with_both_Email_and_Password(email,Password);
        Rishtey_Home_PageObjects.Goto_Claim_entry_page();
        Rishtey_Claim_Entry_Page_Objects Rishtey_Claim_Entry_Page_Objects = PageFactory.initElements(getDriver(), Rishtey_Claim_Entry_Page_Objects.class);
        Rishtey_Claim_Entry_Page_Objects.Check_Elements_In_Claim_Entry_Page();
    }


    //Valid claim adding and Checking the alert
    @Test(dataProviderClass = Staticprovider.class,dataProvider = "RishteyData")

    public void Valid_Save_claim_alert_check(String Admin, String AdminPwd,String SchAdmin,String schPassword,String email, String Password,String invoiceNumber,String amount,String  date,String distrib,String Datenov) throws InterruptedException
    {
        Rishtey_Home_PageObjects Rishtey_Home_PageObjects = PageFactory.initElements(getDriver(), Rishtey_Home_PageObjects.class);
        Rishtey_Login_PageObjects rishteyLoginPageObjects=PageFactory.initElements(getDriver(),Rishtey_Login_PageObjects.class);
        rishteyLoginPageObjects.Navigate_to_baseURL();
        rishteyLoginPageObjects.Logout();
        rishteyLoginPageObjects.Login_with_both_Email_and_Password(email,Password);
        Rishtey_Home_PageObjects.Goto_Claim_entry_page();
        Rishtey_Claim_Entry_Page_Objects Rishtey_Claim_Entry_Page_Objects = PageFactory.initElements(getDriver(), Rishtey_Claim_Entry_Page_Objects.class);
        Rishtey_Claim_Entry_Page_Objects.Entering_valid_claim_and_save(invoiceNumber,amount,date,distrib);
    }
    @Test(dataProviderClass = Staticprovider.class,dataProvider = "RishteyData")

    public void it_should_not_accept_claim_date_before_November1st(String Admin, String AdminPwd,String SchAdmin,String schPassword,String email, String Password,String invoiceNumber,String amount,String  date,String distrib,String DateNov) throws InterruptedException
    {
        Rishtey_Home_PageObjects Rishtey_Home_PageObjects = PageFactory.initElements(getDriver(), Rishtey_Home_PageObjects.class);
        Rishtey_Login_PageObjects rishteyLoginPageObjects=PageFactory.initElements(getDriver(),Rishtey_Login_PageObjects.class);
        rishteyLoginPageObjects.Navigate_to_baseURL();
        rishteyLoginPageObjects.Logout();
        rishteyLoginPageObjects.Login_with_both_Email_and_Password(email, Password);
        Rishtey_Home_PageObjects.Goto_Claim_entry_page();
        Rishtey_Claim_Entry_Page_Objects Rishtey_Claim_Entry_Page_Objects = PageFactory.initElements(getDriver(), Rishtey_Claim_Entry_Page_Objects.class);
        Rishtey_Claim_Entry_Page_Objects.clear_Date();
        Rishtey_Claim_Entry_Page_Objects.Entering_Invalid_date_claim_and_save(invoiceNumber,amount,DateNov,distrib);
    }


    public void it_should_add_points_after_adding_claim()
    {
        Rishtey_Home_PageObjects Rishtey_Home_PageObjects = PageFactory.initElements(getDriver(), Rishtey_Home_PageObjects.class);

    }


}

