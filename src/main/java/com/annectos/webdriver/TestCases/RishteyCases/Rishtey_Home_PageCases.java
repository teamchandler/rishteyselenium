package com.annectos.webdriver.TestCases.RishteyCases;


import com.annectos.webdriver.Common.Staticprovider;
import com.annectos.webdriver.Common.Testbase;
import com.annectos.webdriver.PageObjects.RishteyClaim.Rishtey_Home_PageObjects;
import com.annectos.webdriver.PageObjects.RishteyClaim.Rishtey_Login_PageObjects;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import java.text.ParseException;

/**
 * Created with IntelliJ IDEA.
 * User: praveen
 * Date: 6/24/14
 * Time: 6:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class Rishtey_Home_PageCases extends Testbase {

    @Test(dataProviderClass = Staticprovider.class,dataProvider="RishteyData")
    public void it_should_dispaly_all_the_menus(String Admin, String AdminPwd, String SchAdmin, String schPassword, String email, String Password, String invoiceNumber, String amount, String date, String distrib, String Datenov) throws InterruptedException, ParseException {


        if(threadDriver!=null)
        {
           // findRemoteip(threadDriver.get());
        }
        Rishtey_Home_PageObjects rishteyHomePageObjects = PageFactory.initElements(getDriver(), Rishtey_Home_PageObjects.class);
        Rishtey_Login_PageObjects rishteyLoginPageObjects=PageFactory.initElements(getDriver(),Rishtey_Login_PageObjects.class);
        rishteyLoginPageObjects.Navigate_to_baseURL();
        rishteyLoginPageObjects.Login_with_both_Email_and_Password(email,Password);
        rishteyHomePageObjects.checkAllMenus();




    }
}
