package com.annectos.webdriver.TestCases.RishteyCases;

import com.annectos.webdriver.Common.Staticprovider;
import com.annectos.webdriver.Common.Testbase;

import com.annectos.webdriver.PageObjects.RishteyClaim.Rishtey_Home_PageObjects;
import com.annectos.webdriver.PageObjects.RishteyClaim.Rishtey_Login_PageObjects;
import com.annectos.webdriver.PageObjects.RishteyClaim.Rishtey_Contact_PageObjects;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import java.text.ParseException;

/**
 * Created by praveen on 7/9/2014.
 */
public class Rishtey_Contact_PageCases extends Testbase {

    @Test(dataProviderClass = Staticprovider.class,dataProvider="RishteyData")
    public void Contact_page_should_show_all_contact_details(String Admin, String AdminPwd, String SchAdmin, String schPassword, String email, String Password, String invoiceNumber, String amount, String date, String distrib, String Datenov) throws InterruptedException, ParseException {
        Rishtey_Home_PageObjects rishteyHomePageObjects= PageFactory.initElements(getDriver(), Rishtey_Home_PageObjects.class);
        Rishtey_Login_PageObjects rishteyLoginPageObjects=PageFactory.initElements(getDriver(),Rishtey_Login_PageObjects.class);
        rishteyLoginPageObjects.Navigate_to_baseURL();
        rishteyLoginPageObjects.Logout();
        rishteyLoginPageObjects.Login_with_both_Email_and_Password(email,Password);
        rishteyHomePageObjects.Gotto_Conact_page();
        Rishtey_Contact_PageObjects rishtey_contact_pageObjects = PageFactory.initElements(getDriver(), Rishtey_Contact_PageObjects.class);
        rishtey_contact_pageObjects.Checkall_labels();
    }
}
