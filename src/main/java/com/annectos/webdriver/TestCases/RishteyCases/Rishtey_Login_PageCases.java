package com.annectos.webdriver.TestCases.RishteyCases;

import com.annectos.webdriver.Common.Staticprovider;
import com.annectos.webdriver.Common.Testbase;
import com.annectos.webdriver.PageObjects.RishteyClaim.Rishtey_Login_PageObjects;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import java.text.ParseException;

/**
 * Created with IntelliJ IDEA.
 * User: praveen
 * Date: 6/24/14
 * Time: 12:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class Rishtey_Login_PageCases extends Testbase {

    @Test(dataProviderClass = Staticprovider.class,dataProvider="RishteyData")
    public void it_should_display_all_the_menus(String Admin, String AdminPwd,String SchAdmin,String schPassword,String email, String Password,String invoiceNumber,String amount,String  date,String distrib,String Datenov) throws InterruptedException, ParseException {


        if(threadDriver!=null)
        {
         //   findRemoteip(threadDriver.get());
        }
        Rishtey_Login_PageObjects rishteyLoginPageObjects=PageFactory.initElements(getDriver(),Rishtey_Login_PageObjects.class);
        rishteyLoginPageObjects.Navigate_to_baseURL();
        rishteyLoginPageObjects.checkAllMenus();
    }


    @Test(dataProviderClass = Staticprovider.class,dataProvider="RishteyData")
    public void it_should_give_an_alert_saying_Credentials_dont_match(String Admin, String AdminPwd,String SchAdmin,String schPassword,String email, String Password,String invoiceNumber,String amount,String  date,String distrib,String Datenov) throws InterruptedException, ParseException {


        if(threadDriver!=null)
        {
//            findRemoteip(threadDriver.get());
        }
    {
        Rishtey_Login_PageObjects rishteyLoginPageObjects=PageFactory.initElements(getDriver(),Rishtey_Login_PageObjects.class);
        rishteyLoginPageObjects.Navigate_to_baseURL();
        rishteyLoginPageObjects.Login_without_password(email);

    }

    }

    @Test(dataProviderClass = Staticprovider.class,dataProvider="RishteyData")
    public void it_should_give_an_alert_saying_Credentials_dont_match_(String Admin, String AdminPwd, String SchAdmin, String schPassword, String email, String Password, String invoiceNumber, String amount, String date, String distrib, String Datenov) throws InterruptedException, ParseException {


        if(threadDriver!=null)
        {
//            findRemoteip(threadDriver.get());
        }
        {
            Rishtey_Login_PageObjects rishteyLoginPageObjects=PageFactory.initElements(getDriver(),Rishtey_Login_PageObjects.class);
            rishteyLoginPageObjects.Navigate_to_baseURL();
            rishteyLoginPageObjects.Login_without_email(Password);

        }

    }

    @Test(dataProviderClass = Staticprovider.class,dataProvider="RishteyData")
    public void it_should_Login_successfully_(String Admin, String AdminPwd,String SchAdmin,String schPassword,String email, String Password,String invoiceNumber,String amount,String  date,String distrib,String Datenov) throws InterruptedException, ParseException {


        if(threadDriver!=null)
        {
//            findRemoteip(threadDriver.get());
        }
        {
            Rishtey_Login_PageObjects rishteyLoginPageObjects=PageFactory.initElements(getDriver(),Rishtey_Login_PageObjects.class);
            rishteyLoginPageObjects.Navigate_to_baseURL();
            rishteyLoginPageObjects.Login_with_both_Email_and_Password(email, Password);
            rishteyLoginPageObjects.Profile();
            rishteyLoginPageObjects.Logout();

        }

    }

}
