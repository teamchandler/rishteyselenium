package com.annectos.webdriver.TestCases.RishteyCases;

import com.annectos.webdriver.Common.Staticprovider;
import com.annectos.webdriver.Common.Testbase;
import com.annectos.webdriver.PageObjects.RishteyClaim.*;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by praveen on 7/7/2014.
 */
public class Rishtey_ProgramAdmin_PageCases extends Testbase{

    @Test(dataProviderClass = Staticprovider.class,dataProvider="SeepAdminLogin")
    public void it_should_display_all_the_menus(String Admin, String AdminPwd,String SchAdmin,String schPassword) throws Exception {


        if(threadDriver!=null)
        {
            // findRemoteip(threadDriver.get());
        }
        Rishtey_Login_PageObjects rishteyLoginPageObjects=PageFactory.initElements(getDriver(),Rishtey_Login_PageObjects.class);
        rishteyLoginPageObjects.Navigate_to_baseURL();
        rishteyLoginPageObjects.Logout();
        rishteyLoginPageObjects.Login_with_both_Email_and_Password(SchAdmin, schPassword);
        rishteyLoginPageObjects.elmPROGRAMADMINMenu.click();
        Rishtey_Admin_Program_PageObjects rishtey_adminProgramPageObjects =PageFactory.initElements(getDriver(),Rishtey_Admin_Program_PageObjects.class);
        rishtey_adminProgramPageObjects.CheckRadio_buttons();
        rishtey_adminProgramPageObjects.Checkall_labels();
        rishteyLoginPageObjects.Logout();


    }

    @Test(dataProviderClass = Staticprovider.class,dataProvider="RishteyData",enabled = false)
    public void Schneider_verification_required(String Admin, String AdminPwd,String SchAdmin,String schPassword,String email, String Password,String invoiceNumber,String amount,String  date,String distrib,String DateNov) throws InterruptedException{


        if(threadDriver!=null)
        {
            // findRemoteip(threadDriver.get());
        }
        Rishtey_Home_PageObjects rishteyHomePageObjects= PageFactory.initElements(getDriver(), Rishtey_Home_PageObjects.class);
        Rishtey_Login_PageObjects rishteyLoginPageObjects=PageFactory.initElements(getDriver(),Rishtey_Login_PageObjects.class);
        rishteyLoginPageObjects.Navigate_to_baseURL();
        rishteyLoginPageObjects.Logout();
        rishteyLoginPageObjects.Login_with_both_Email_and_Password(email,Password);
        rishteyLoginPageObjects.elmPROGRAMADMINMenu.click();
        Rishtey_Admin_Program_PageObjects rishtey_adminProgramPageObjects =PageFactory.initElements(getDriver(),Rishtey_Admin_Program_PageObjects.class);
        rishtey_adminProgramPageObjects.CheckRadio_buttons();
        rishtey_adminProgramPageObjects.Checkall_labels();
        rishtey_adminProgramPageObjects.Select_Schneider_Verification_Required_radio();
        rishtey_adminProgramPageObjects.Select_All_radio();


    }


    //Logs in As user add a new claim,Logout,Login as Annectos Admin
    // and go to program admin page check the Invoice details like Date,Amount,Date and Distributor by clicking the details link
    @Test(dataProviderClass = Staticprovider.class,dataProvider="RishteyData")

    public void  it_should_showCorrect_Claim_Datas_in_Admin_Login_As_entered_by_user(String Admin, String AdminPwd,String SchAdmin,String schPassword,String email, String Password,String invoiceNumber,String amount,String  date,String distrib,String DateNov) throws InterruptedException, ParseException {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date dte = dateFormat.parse(date);
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MMMMMMM");
        String finaldate=dateFormat1.format(dte);

        Rishtey_Home_PageObjects rishteyHomePageObjects= PageFactory.initElements(getDriver(), Rishtey_Home_PageObjects.class);
        Rishtey_Login_PageObjects rishteyLoginPageObjects=PageFactory.initElements(getDriver(),Rishtey_Login_PageObjects.class);
        rishteyLoginPageObjects.Navigate_to_baseURL();
        rishteyLoginPageObjects.Logout();
        rishteyLoginPageObjects.Login_with_both_Email_and_Password(email,Password);
        rishteyHomePageObjects.Goto_Claim_entry_page();
        Rishtey_Claim_Entry_Page_Objects claim_entry_page_objects=PageFactory.initElements(getDriver(),Rishtey_Claim_Entry_Page_Objects.class);
        claim_entry_page_objects.Entering_valid_claim_and_save(invoiceNumber,amount,finaldate,distrib) ;
        rishteyLoginPageObjects.Logout();
        Rishtey_Admin_Program_PageObjects rishtey_adminProgramPageObjects =PageFactory.initElements(getDriver(),Rishtey_Admin_Program_PageObjects.class);
        rishteyLoginPageObjects.Login_with_both_Email_and_Password(Admin,AdminPwd);
        rishteyLoginPageObjects.elmPROGRAMADMINMenu.click();
        rishtey_adminProgramPageObjects.Select_All_radio();
        rishtey_adminProgramPageObjects.check_entered_claim_shows(amount);
        rishtey_adminProgramPageObjects.Click_Details_of_the_Specific_Invoice(amount);
        Rishtey_Claim_Details_AnnAdmin_Objects rishteyClaimDetailsAnnAdminObjects=PageFactory.initElements(getDriver(),Rishtey_Claim_Details_AnnAdmin_Objects.class);
       // rishteyClaimDetailsAnnAdminObjects.assert_Correct_Date_Is_showing(date);
        rishteyClaimDetailsAnnAdminObjects.assert_Correct_Invoice_amount_is_showing(amount);
        rishteyClaimDetailsAnnAdminObjects.assert_Correct_Invoice_Number_is_showing(invoiceNumber);
        rishteyClaimDetailsAnnAdminObjects.assert_Correct_Distributor_Is_showing(distrib);



    }


}

