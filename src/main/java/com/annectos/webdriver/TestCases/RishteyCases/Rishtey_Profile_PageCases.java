package com.annectos.webdriver.TestCases.RishteyCases;

import com.annectos.webdriver.Common.Staticprovider;
import com.annectos.webdriver.Common.Testbase;
import com.annectos.webdriver.PageObjects.RishteyClaim.Rishtey_Login_PageObjects;
import com.annectos.webdriver.PageObjects.RishteyClaim.Rishtey_Profile_PageObject;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import java.text.ParseException;

/**
 * Created with IntelliJ IDEA.
 * User: praveen
 * Date: 6/24/14
 * Time: 6:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class Rishtey_Profile_PageCases extends Testbase{

    @Test(dataProviderClass = Staticprovider.class,dataProvider="RishteyData")
    public void all_element_present(String email, String Password ) throws Exception

    {
        if(threadDriver!=null)
        {
            //findRemoteip(threadDriver.get());
        }
        {
            Rishtey_Login_PageObjects rishteyLoginPageObjects= PageFactory.initElements(getDriver(), Rishtey_Login_PageObjects.class);
            Rishtey_Profile_PageObject rishteyProfilePageObject =PageFactory.initElements(getDriver(),Rishtey_Profile_PageObject.class);
            rishteyLoginPageObjects.Navigate_to_baseURL();
            rishteyLoginPageObjects.Login_with_both_Email_and_Password(email,Password);
            rishteyLoginPageObjects.Profile();
            rishteyProfilePageObject.checkAllElements();
            rishteyLoginPageObjects.Logout();

        }
    }

    @Test(dataProviderClass = Staticprovider.class,dataProvider="RishteyData")
    public void It_should_give_alert_to_enter_first_name(String Admin, String AdminPwd,String SchAdmin,String schPassword,String email, String Password,String invoiceNumber,String amount,String  date,String distrib,String Datenov) throws InterruptedException, ParseException

    {
        if(threadDriver!=null)
        {
            //findRemoteip(threadDriver.get());
        }
        {
            Rishtey_Login_PageObjects rishteyLoginPageObjects= PageFactory.initElements(getDriver(), Rishtey_Login_PageObjects.class);
            Rishtey_Profile_PageObject rishteyProfilePageObject =PageFactory.initElements(getDriver(),Rishtey_Profile_PageObject.class);
            rishteyLoginPageObjects.Navigate_to_baseURL();
            rishteyLoginPageObjects.Login_with_both_Email_and_Password(email,Password);
            rishteyLoginPageObjects.Profile();
            rishteyProfilePageObject.Save_profile_without_First_name("Prsveen","Kumar","9886869680","11/11/1985","11/11/1900","B.E");
            rishteyLoginPageObjects.Logout();

        }
    }

}
