package com.annectos.webdriver.TestCases.Promo;

import com.annectos.webdriver.Common.Staticprovider;
import com.annectos.webdriver.Common.Testbase;
import com.annectos.webdriver.PageObjects.Promo.SamplePromoPAgeObjects;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

/**
 * Created by praveen on 11/10/2014.
 */
public class SamplePromoTestCases extends Testbase {

    @Test(dataProviderClass = Staticprovider.class,dataProvider="PromoData")
    public void Claim_tee(String firstnamepromo, String lastnamepromo,String companypromo, String designationpromo,
                                                String eamilidpromo, String phonepromo,String addressLine1promo ,
                                                String addressLine2promo,String zipcodepromo) throws Exception {


        if(threadDriver!=null)
        {
            // findRemoteip(threadDriver.get());
        }
        SamplePromoPAgeObjects samplePromoPAgeObjects = PageFactory.initElements(getDriver(), SamplePromoPAgeObjects.class);
        samplePromoPAgeObjects.Goto_Promo();
        samplePromoPAgeObjects.Click_image1();

        samplePromoPAgeObjects.Fill_Claim_tee_Details_without_Size(firstnamepromo,lastnamepromo,designationpromo,companypromo,eamilidpromo
        ,phonepromo,addressLine1promo,addressLine2promo,zipcodepromo);
        samplePromoPAgeObjects.Click_Claim_Button();


    }
}
