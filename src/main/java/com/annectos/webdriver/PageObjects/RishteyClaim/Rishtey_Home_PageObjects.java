package com.annectos.webdriver.PageObjects.RishteyClaim;

import com.annectos.webdriver.Common.CommonMethods;
import com.annectos.webdriver.Helper.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created with IntelliJ IDEA.
 * User: praveen
 * Date: 6/24/14
 * Time: 5:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class Rishtey_Home_PageObjects extends CommonMethods {

    @FindBy(partialLinkText = "HOME")
    public WebElement elmHomeMenu;

    @FindBy(partialLinkText = "PROFILE")
    public  WebElement elmPROFILEMenu;

    @FindBy(partialLinkText = "RISHTEY")
    public  WebElement elmRishteyMenu;

    @FindBy(partialLinkText = "CLAIM ENTRY")
    public  WebElement elmCLAIMENTRYMenu;

    @FindBy(partialLinkText = "MY CLAIMS")
    public  WebElement elmMYCLAIMSMenu;

    @FindBy(partialLinkText = "REDEMPTION")
    public  WebElement elMREDEMPTIONMenu;

    @FindBy(partialLinkText = "REWARD GALLERY")
    public  WebElement elmREWARDMenu;

    @FindBy(partialLinkText = "CONTACT")
    public  WebElement elmCONTACTMenu;

    @FindBy(partialLinkText = "SPECIAL")
    public  WebElement elmSPECIALMenu;

    @FindBy(partialLinkText = "CHANGE PASSWORD")
    public  WebElement elmCHNGPWDMenu;

    @FindBy(partialLinkText = "LOGOUT")
    public  WebElement elmLOGOUTMenu;

    @FindBy(css = "footer")
    public  WebElement elmFooter;

    @FindBy(css="a.left.carousel-control")
    public WebElement elmLeftarrow;

    @FindBy(css="a.right.carousel-control")
    public WebElement elmRightarrow;

    @FindBy(xpath = "//div[@class='carousel-inner']/div[2]/a/img")
    public WebElement elmImageBird;

    @FindBy(xpath = "//div[@class='carousel-inner']/div[1]/a/img")
    public WebElement elmImageGrowing;

    @FindBy(css="img.pull-left")
    public WebElement elmLogoLeft;

    @FindBy(css="img.pull-right")
    public WebElement elmLogoRight;


    public Rishtey_Home_PageObjects(WebDriver adriver) {
        super(adriver);
        driver = adriver;
        wait = new WebDriverWait(driver, timeOut);
        driver.manage().window().maximize();
    }
    public void checkAllMenus()
    {

        Logger.Log(LOG_FILE,"checkAllMenus", "Checking all elements are present in the Home Page Rishtey ",driver,true);

        assertElementPresent(elmRishteyMenu,"Rishtey Menu");

        assertElementPresent(elmHomeMenu,"Home Menu");
        assertElementPresent(elmPROFILEMenu,"Profile Menu");
        assertElementPresent(elmRishteyMenu,"Rishtey Menu");
        assertElementPresent(elmCLAIMENTRYMenu,"Claim Entry Menu");
        assertElementPresent(elmMYCLAIMSMenu,"My Claims Menu");
        assertElementPresent(elmREWARDMenu,"Rewards Menu");
        assertElementPresent(elmPROFILEMenu,"Profile Menu");
        assertElementPresent(elmCONTACTMenu,"Contact Menu");
        assertElementPresent(elmSPECIALMenu,"Special Menu");
        assertElementPresent(elmCHNGPWDMenu,"Change Password Menu");
        assertElementPresent(elmLOGOUTMenu,"Logout Menu");
        assertElementPresent(elmImageGrowing,"Scrolling Image");
        assertElementPresent(elmImageBird,"Scrolling Image");
        assertElementPresent(elmLeftarrow,"Left Arrow");
        assertElementPresent(elmRightarrow,"Right Arrow");
        assertElementPresent(elmFooter,"Footer") ;
        assertElementPresent(elmLogoLeft,"Logo in Left");
        assertElementPresent(elmLogoRight,"Logo in Left");
    }

    public void Navigate_to_baseURL()
    {
        driver.navigate().to(baseURL);
    }


    //Checks The page Title

    public void verify_page_title(String title)
    {

        Assert_Page_Title(title);
        Logger.Log(LOG_FILE, "Seep Login Page title Check ", "The Title matches ", driver, true);

    }

    //Clicks on Claim entry Menu

    public void Goto_Claim_entry_page()
    {
        WaitforElementToLoadAndClick(elmCLAIMENTRYMenu, "Submit btn");
    }

    //Clicks on Contact Menu

    public void Gotto_Conact_page(){WaitforElementToLoadAndClick(elmCONTACTMenu, "Submit btn");}

    //Clicks on Logout Menu

    public void Logout() {

        WaitforElementToLoadAndClick(elmLOGOUTMenu, "Submit btn");
    }

}
