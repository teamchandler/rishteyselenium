package com.annectos.webdriver.PageObjects.RishteyClaim;


import com.annectos.webdriver.Common.CommonMethods;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

/**
 * Created by praveen on 7/16/2014.
 */
public class Rishtey_Claim_Details_AnnAdmin_Objects extends CommonMethods {

    public Rishtey_Claim_Details_AnnAdmin_Objects(WebDriver adriver) {
        super(adriver);
        driver = adriver;
        wait = new WebDriverWait(driver, timeOut);
        driver.manage().window().maximize();
    }

    @FindBy(css="div.pagetitle")
    public WebElement pagetitle;

    @FindBy(css = "div > div:nth-child(2) > div > div > div:nth-child(1) > p:nth-child(1) > label")
    public WebElement elmLabelInvoiceNo;

    @FindBy(css = "Input[ng-model=\"inv_detail.invoice_number\"]")
    public WebElement elmtxInvoiceNumber;

    @FindBy(css = "div > div:nth-child(2) > div > div > div:nth-child(2) > p:nth-child(1) > label")
    public WebElement elmLabelInvoiceDate;

    @FindBy(css = "Input[ng-model=\"invoice_date\"]")
    public WebElement elmTxtInvoiceDate;

    @FindBy(css = "div > div:nth-child(2) > div > div > div:nth-child(1) > p:nth-child(2) > label")
    public WebElement elmLabelInvoiceAmount;

    @FindBy(css = "Input[ng-model=\"inv_detail.invoice_amount\"]")
    public WebElement elmTxtInvoiceAmount;

    @FindBy(css = "div > div:nth-child(2) > div > div > div:nth-child(2) > p:nth-child(2) > label")
    public WebElement elmLabelDistributor;

    @FindBy(css = "select[ng-model=\"inv_detail.retailer\"]")
    public WebElement elmInvoice_Distributor;

    @FindBy(css = "pagetitle tab_head")
    public WebElement elmLabelpartCodeDetails;

    @FindBy(css = "div > div:nth-child(4) > div > div:nth-child(1) > div > p > label")
    public WebElement elmLabelPartCODE;

    @FindBy(css = "Input[ng-model=\"sku_data\"]\")")
    public WebElement elmPartcode;

    @FindBy(css = "Input[type='button'][value='Add']")
    public WebElement elmbtAddSku;

    @FindBy(css = "div > div:nth-child(5) > table > tbody > tr > td:nth-child(1)")
    public WebElement elmTableHdrPartCode;

    @FindBy(css = "div > div:nth-child(5) > table > tbody > tr > td:nth-child(2)")
    public WebElement elmTableHdrStatus;

    @FindBy(css = "div > div:nth-child(5) > table > tbody > tr > td:nth-child(3)")
    public WebElement elmTableHdrAction;

    @FindBy(css = "div.ng-scope > div > div:nth-child(6) > div")
    public WebElement elmLabelCommentHistory;

    @FindBy(css = "div > div:nth-child(7) > table > tbody > tr > td:nth-child(1)")
    public WebElement elmLabelDate;

    @FindBy(css = "div > div:nth-child(7) > table > tbody > tr > td:nth-child(2)")
    public WebElement elmLabelTime;

    @FindBy(css = "div > div:nth-child(7) > table > tbody > tr > td:nth-child(3)")
    public WebElement elmLabelUser;

    @FindBy(css = "div > div:nth-child(7) > table > tbody > tr > td:nth-child(4)")
    public WebElement elmLabelPreviousComments;

    @FindBy(css = "textarea.ng-pristine.ng-valid")
    public WebElement elmCommentArea;



    public void Check_all_labels()
    {
        assertLabelText("CLAIM DETAILS",pagetitle);
        assertLabelText("Invoice No",elmLabelInvoiceNo);
        assertLabelText("Invoice Date",elmLabelInvoiceDate);
        assertLabelText("Invoice Amount",elmLabelInvoiceAmount);
        assertLabelText("Distributor",elmLabelDistributor);
        assertLabelText("Part CODE DETAILS",elmLabelpartCodeDetails);
        assertLabelText("Part CODE",elmLabelPartCODE);

        assertLabelText("STATUS",elmTableHdrStatus);
        assertLabelText("ACTION",elmTableHdrAction);
        assertLabelText("Part CODE",elmTableHdrPartCode);

        assertLabelText("Comment History",elmLabelCommentHistory);
        assertLabelText("Date",elmLabelDate);
        assertLabelText("Time",elmLabelTime);
        assertLabelText("User",elmLabelUser);
        assertLabelText("Previous Comments",elmLabelPreviousComments);


    }

    public String Get_Selected_distributor()
    {
        waitforElement_to_Load(elmInvoice_Distributor, "Invoice Distributor Field");
        Select select = new Select(elmInvoice_Distributor);
        String Distributor=select.getFirstSelectedOption().getText();
        return Distributor;
    }

    public String Get_Invoice_number()
    {
        waitforElement_to_Load(elmtxInvoiceNumber, "Invoice Number Field");
        String Invoicenumber=elmtxInvoiceNumber.getAttribute("value");
        return Invoicenumber;
    }


    public String Get_Invoice_date()
    {
        waitforElement_to_Load(elmTxtInvoiceDate, "Invoice Date Field");
        String date=elmTxtInvoiceDate.getAttribute("value");
        return date;
    }

    public String Get_Invoice_amount()
    {
        waitforElement_to_Load(elmTxtInvoiceAmount, "Invoice Field");
        String amount=elmTxtInvoiceAmount.getAttribute("value");
        return amount;
    }

    public void assert_Correct_Invoice_amount_is_showing(String amount)
    {
        System.out.println(Get_Invoice_amount());
        Assert.assertEquals(amount,Get_Invoice_amount());
    }

    public void assert_Correct_Date_Is_showing(String date)
    {
        System.out.println(Get_Invoice_date());
        Assert.assertEquals(date,Get_Invoice_date());
    }

    public void assert_Correct_Invoice_Number_is_showing(String InvoiceNo)
    {
        System.out.println(Get_Invoice_number());
        Assert.assertEquals(InvoiceNo,Get_Invoice_number());
    }

    public void assert_Correct_Distributor_Is_showing(String Distributor)
    {
        System.out.println(Get_Selected_distributor());
        Assert.assertEquals(Distributor,Get_Selected_distributor());
    }

    public void Add_Part_code(String Partcode)
    {
      typePartcode(Partcode);
      Click_add_button();

    }

    public void Check_Part_Code_Status()
    {

    }

    public void typePartcode(String PartCode)
    {
        sendtext(elmPartcode,PartCode);
    }

    public void Click_add_button()
    {
        WaitforElementToLoadAndClick(elmbtAddSku, "Submit btn");
    }


}
