package com.annectos.webdriver.PageObjects.RishteyClaim;

import com.annectos.webdriver.Common.CommonMethods;
import com.annectos.webdriver.Helper.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by praveen on 11/12/2014.
 */
public class Rishtey_Claim_Entry_Page_Objects extends CommonMethods {

    @FindBy(css = "Input[ng-model=\"invo_data.invoice_number\"]")
    public WebElement elm_ClaimEntry_Invoice_No;

    @FindBy(css = "Input[ng-model=\"invo_data.invoice_amount\"]")
    public WebElement elm_ClaimEntry_Invoice_Amount;

    @FindBy(css = "select[ng-model=\"invo_data.retailer\"]")
    public WebElement elm_ClaimEntry_Invoice_Distributor;

    @FindBy(xpath = "//input[@type='button' and @value='Add New Claim']")
    public WebElement elem_ClaimEntry_addNewClaim;

    @FindBy(xpath = "//input[@type='button' and @value='Cancel']")
    public WebElement elem_ClaimEntry_cancel;

    @FindBy(xpath = "//input[@type='submit' and @value='Save']")
    public WebElement elem_ClaimEntry_Save;

    @FindBy(xpath="/html/body/div[2]/div/div[2]/div/div/form/div/div[2]/p[3]/input")
    public WebElement Upload_file;

    @FindBy(xpath = "//div[@class='tablecolorborder']//label[.='Invoice No *']")
    public WebElement label_claimEntry_InvoiceNumber;

    @FindBy(xpath = "//div[@class='tablecolorborder']//label[.='Invoice Date *']")
    public WebElement label_claimEntry_InvoiceDateLabel;

    @FindBy(xpath = "//div[@class='tablecolorborder']//label[.='Distributor *']")
    public WebElement label_claimEntry_InvoiceDistributorLabel;

    @FindBy(xpath = "//div[@class='tablecolorborder']//label[.='Invoice Amount *']")
    public WebElement label_claimEntry_InvoiceAmountLabel;

    @FindBy(css="p > span > button.btn.btn-default")
    public WebElement elm_ClaimEntry_Invoice_Date;

    @FindBy(css="p:nth-child(1) > ul > li:nth-child(1) > table > thead > tr:nth-child(1) > th:nth-child(2) > button")
    public WebElement elm_ClaimEntry_Invoice_Date_month_picker;

    @FindBy(xpath = "//span[@class='btn-group']//button[.='Clear']")
    public WebElement elm_ClaimEntry_Invoice_Date_Clear;



    @FindBy(xpath="//table[@class='ng-isolate-scope']//button[.='25']\n")
    public WebElement elm_ClaimEntry_Invoice_Date_pic;


    public Rishtey_Claim_Entry_Page_Objects(WebDriver adriver) {
        super(adriver);
        driver = adriver;
        wait = new WebDriverWait(driver, timeOut);
        driver.manage().window().maximize();



    }
    //Checks all elements are present in Claim Entry Page

    public void Check_Elements_In_Claim_Entry_Page()


    {
        Logger.Log(LOG_FILE,"Check_Elements_In_Claim_Entry_Page","Inside the method which Checks For Fields nad Labels in ClaimEntryPage",driver,true);

        VerifyElementPresent(elm_ClaimEntry_Invoice_No,"Invoice number field");
        VerifyElementPresent(elm_ClaimEntry_Invoice_Date, "Invoice date field");
        VerifyElementPresent(elm_ClaimEntry_Invoice_Amount, "Invoice amount field") ;
        VerifyElementPresent(elem_ClaimEntry_addNewClaim, "Invoice add new claim btn");
        VerifyElementPresent(elem_ClaimEntry_cancel, "cancel btn");
        VerifyElementPresent(elem_ClaimEntry_Save, "Save btn");
        VerifyElementPresent(label_claimEntry_InvoiceNumber, "Invoice number label");
        VerifyElementPresent(label_claimEntry_InvoiceDateLabel, "Invoice date label");
        VerifyElementPresent(label_claimEntry_InvoiceDistributorLabel, "Invoice Distributor label");
        VerifyElementPresent(label_claimEntry_InvoiceAmountLabel, "Invoice amount  label");

    }

    //Sets Invoice Number
    public void Enter_Invoice_number(String invoicenumber)
    {
        SetText(elm_ClaimEntry_Invoice_No,invoicenumber,"Invoice number");
        Logger.Log(LOG_FILE, "Enter_Invoice_number", "Entering Invoice number" + invoicenumber, driver, true);

    }

    //Sets Invoice Date
    public void   Enter_Invoice_Date(String invoiceDate)

    {
        String mnth=invoiceDate.substring(invoiceDate.lastIndexOf("/") + 1);
        String dte=invoiceDate.replaceAll("\\D+","");
        System.out.println(mnth);
        System.out.println(dte);
        WaitforElementToLoadAndClick(elm_ClaimEntry_Invoice_Date, "Submit btn");
        WaitforElementToLoadAndClick(elm_ClaimEntry_Invoice_Date_month_picker, "Submit btn");
        WebElement month=driver.findElement(By.cssSelector("div > div:nth-child(2) > p:nth-child(1) > ul > li:nth-child(1) > table > tbody > tr:nth-child(4) > td:nth-child(2) > button > span"));
        month.click();
        WebElement date=driver.findElement(By.cssSelector("body > div.ng-scope > div:nth-child(2) > div:nth-child(2) > div > div > form > div > div:nth-child(2) > p:nth-child(1) > ul > li:nth-child(1) > table > tbody > tr:nth-child(4) > td:nth-child(4) > button > span"));
        date.click();
        Logger.Log(LOG_FILE,"Enter_Invoice_Date","Entering Invoice date"+invoiceDate,driver,true);

    }

    public void clear_Date()
    {
        WaitforElementToLoadAndClick(elm_ClaimEntry_Invoice_Date, "Submit btn");
        WaitforElementToLoadAndClick(elm_ClaimEntry_Invoice_Date_Clear, "Submit btn");
    }

    //Sets Invoice Amount
    public void Enter_Invoice_Amount(String invoiceAmount)

    {
        Logger.Log(LOG_FILE,"Enter_Invoice_Amount","This will fill invoice amount "+invoiceAmount,driver,true);
        SetText(elm_ClaimEntry_Invoice_Amount,invoiceAmount,"Invoice Amount");

    }

    //Sets Distributor

    public void Select_distributor(String Distributor)
    {
        waitforElement_to_Load(elm_ClaimEntry_Invoice_Distributor, "Distributor Field");
        Select select = new Select(elm_ClaimEntry_Invoice_Distributor);
        select.selectByValue(Distributor);
        Logger.Log(LOG_FILE,"Select_distributor ","Selecting distributor from drop down"+Distributor,driver,true);


    }

    //Clicks on Save Claim
    public void SaveClaim()
    {
        waitforElement_to_Load(elem_ClaimEntry_Save, "Claim Save Button") ;
        WaitForElemntTobeEnabled(elem_ClaimEntry_Save);
        WaitforElementToLoadAndClick(elem_ClaimEntry_Save, "Submit btn");


    }

    //Verifies alert when Invoice number is left blank
    public void verify_alert_When_invoice_number_left_blank(String invoiceNumber,String amount,String  date,String distrib)
    {
        Enter_Invoice_number("");
        Enter_Invoice_Date(date);

        Enter_Invoice_Amount(amount);
        Select_distributor(distrib);
        System.out.println(userdir);
        file_upload(Upload_file, userdir + "\\src\\main\\resources\\sample.pdf");
        SaveClaim();
        waitForAlertAndAccept("One of the required fields is missing. Please make sure that you have provided all required information!");
        Logger.Log(LOG_FILE,"SaveClaim ","Clicking the Save button",driver,true);

    }

    //Verifies alert when Invoice date is left blank
    public void verify_alert_When_invoice_date_left_blank(String invoiceNumber,String amount,String  date,String distrib)
    {

        Enter_Invoice_number(invoiceNumber);
        clear_Date();

        Enter_Invoice_Amount(amount);
        Select_distributor(distrib);
        file_upload(Upload_file, userdir + "\\src\\main\\resources\\sample.pdf");
        SaveClaim();
        waitForAlertAndAccept("One of the required fields is missing. Please make sure that you have provided all required information!");
        Logger.Log(LOG_FILE,"SaveClaim ","Clicking the Save button",driver,true);

    }

    //Verifies alert when Invoice amount is left blank
    public void verify_alert_When_invoice_amount_left_blank(String invoiceNumber,String amount,String  date,String distrib)
    {

        Enter_Invoice_number(invoiceNumber);
        Enter_Invoice_Date(date);
        Select_distributor(distrib);
        file_upload(Upload_file,userdir+"\\src\\main\\resources\\sample.pdf");
        SaveClaim();
        waitForAlertAndAccept("One of the required fields is missing.Please make sure that you have provided all the required information!");
    }

    //Verifies alert when Invoice Distributor is left blank
    public void verify_alert_When_Distributor_left_blank(String invoiceNumber,String amount,String  date,String distrib)
    {

        Enter_Invoice_number(invoiceNumber);
        Enter_Invoice_Date(date);

        Enter_Invoice_Amount(amount);
        file_upload(Upload_file,userdir+"\\src\\main\\resources\\sample.pdf");
        SaveClaim();
        waitForAlertAndAccept("One of the required fields is missing.Please make sure that you have provided all the required information!");
    }

    //Enter Valid Claim and Save

    public String Entering_valid_claim_and_save(String invoiceNumber, String amount, String date, String distrib)
    {
        Enter_Invoice_number(invoiceNumber);
        //Enter_Invoice_Date(date);
        Enter_Invoice_Amount(amount);
        Select_distributor(distrib);
        file_upload(Upload_file,userdir+"\\src\\main\\resources\\sample.pdf");
        SaveClaim();
        if(waitForAlertAndAccept("Invoice Details Has Been Successfully Submitted"))
        {
            Logger.Log(LOG_FILE,"Accepted alert","Claim Is Saved Successfully",driver,true);
        }

        return invoiceNumber;

    }

    public String Entering_Invalid_date_claim_and_save(String invoiceNumber, String amount, String date, String distrib)
    {
        Enter_Invoice_number(invoiceNumber);
        Enter_Invoice_Date(date);
        Enter_Invoice_Amount(amount);
        Select_distributor(distrib);
        file_upload(Upload_file,userdir+"\\src\\main\\resources\\sample.pdf");
        SaveClaim();
        if(waitForAlertAndAccept("Invoice date should be in between '01-11-2014' and '31-03-2015'"))
        {
            Logger.Log(LOG_FILE,"Accepted alert","Claim Is Rejected since date is not less than November 11",driver,true);
        }

        return invoiceNumber;

    }






























}
