package com.annectos.webdriver.PageObjects.RishteyClaim;

import com.annectos.webdriver.Common.CommonMethods;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by praveen on 7/9/2014.
 */
public class Rishtey_Contact_PageObjects extends CommonMethods {

    public Rishtey_Contact_PageObjects(WebDriver adriver) {

        super(adriver);
        driver = adriver;
        wait = new WebDriverWait(driver, timeOut);
        driver.manage().window().maximize();
    }

    @FindBy(css = "div.ng-scope > div > div:nth-child(1) > div")
    public WebElement elm_Contact_Label_Contact;

    @FindBy(css="div.ng-scope > div > div.container.middlesection.contents.ng-scope > div > div > li:nth-child(1)")
    public WebElement elm_Contact_Label_AddLine1;


    @FindBy(css="div.ng-scope > div > div.container.middlesection.contents.ng-scope > div > div > li:nth-child(2)")
    public WebElement elm_Contact_Label_AddLine2;


    @FindBy(css="div.ng-scope > div > div.container.middlesection.contents.ng-scope > div > div > li:nth-child(3)")
    public WebElement elm_Contact_Label_AddLine3;


    @FindBy(css="div.ng-scope > div > div.container.middlesection.contents.ng-scope > div > div > li:nth-child(4)")
    public WebElement elm_Contact_Label_AddLine4;

    @FindBy(css="div.ng-scope > div > div.container.middlesection.contents.ng-scope > div > div > li:nth-child(5)")
    public WebElement elm_Contact_Label_AddLine5;

    @FindBy(css="div.ng-scope > div > div.container.middlesection.contents.ng-scope > div > div > li:nth-child(6)")
    public WebElement elm_Contact_Label_AddLine6;

    @FindBy(css="div.ng-scope > div > div.container.middlesection.contents.ng-scope > div > div > li:nth-child(7)")
    public WebElement elm_Contact_Label_AddLine7;



    @FindBy(css="div.ng-scope > div > div.container.middlesection.contents.ng-scope > div > div > li:nth-child(9) > span")
    public WebElement elm_Contact_Label_Time;



    public void Checkall_labels( )
    {
        assertLabelText("CONTACT US",elm_Contact_Label_Contact);
        assertLabelText("Schneider Electric Elite Partner Program",elm_Contact_Label_AddLine1);
        assertLabelText("C/O. annectoś Rewards & Retail Pvt. Ltd.",elm_Contact_Label_AddLine2);
        assertLabelText("No 16 ‐ First Floor,",elm_Contact_Label_AddLine3);
        assertLabelText("Kumara Krupa Road",elm_Contact_Label_AddLine4);
        assertLabelText("Shivananda Circle,", elm_Contact_Label_AddLine5);
        assertLabelText("Bangalore ‐ 560001", elm_Contact_Label_AddLine6);
        assertLabelText("Ph: 9686202046 | 9972334590", elm_Contact_Label_AddLine7);
        assertLabelText("Time: Monday to Friday (Between 9:30 to 6:30 pm)", elm_Contact_Label_Time);
    }
}
