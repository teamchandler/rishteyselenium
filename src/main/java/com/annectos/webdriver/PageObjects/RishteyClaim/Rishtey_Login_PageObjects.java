package com.annectos.webdriver.PageObjects.RishteyClaim;

/**
 * Created with IntelliJ IDEA.
 * User: praveen
 * Date: 6/24/14
 * Time: 12:01 PM
 * To change this template use File | Settings | File Templates.
 */



import com.annectos.webdriver.Common.CommonMethods;
import org.openqa.selenium.support.FindBy;
import com.annectos.webdriver.Helper.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Rishtey_Login_PageObjects extends CommonMethods {

    @FindBy(css = "Input[ng-model=\"email_id\"]")
    public WebElement registeredEMail;

    @FindBy(css = "Input[ng-model=\"password\"]")
    public WebElement password;

    @FindBy(xpath = "//input[@type='submit' and @value='Submit']")
    public WebElement submit;

    @FindBy(partialLinkText = "HOME")
    public WebElement elmHomeMenu;

    @FindBy(partialLinkText = "PROFILE")
    public WebElement elmPROFILEMenu;

    @FindBy(partialLinkText = "RISHTEY")
    public WebElement elmRishteyMenu;

    @FindBy(partialLinkText = "CLAIM ENTRY")
    public WebElement elmCLAIMENTRYMenu;

    @FindBy(partialLinkText = "MY CLAIMS")
    public WebElement elmMYCLAIMSMenu;

    @FindBy(partialLinkText = "REDEMPTION")
    public WebElement elMREDEMPTIONMenu;

    @FindBy(partialLinkText = "REWARD GALLERY")
    public WebElement elmREWARDMenu;

    @FindBy(partialLinkText = "CONTACT")
    public WebElement elmCONTACTMenu;

    @FindBy(partialLinkText = "SPECIAL")
    public WebElement elmSPECIALMenu;

    @FindBy(partialLinkText = "CHANGE PASSWORD")
    public WebElement elmCHNGPWDMenu;

    @FindBy(partialLinkText = "PROG ADM")
    public WebElement elmPROGRAMADMINMenu;


    @FindBy(partialLinkText = "LOGOUT")
    public WebElement elmLOGOUTMenu;

    public Rishtey_Login_PageObjects(WebDriver adriver) {

        super(adriver);
        driver = adriver;
        wait = new WebDriverWait(driver, timeOut);
        driver.manage().window().maximize();

    }

    public void Navigate_to_baseURL() {



        Open_the_url(baseURL)  ;

    }

    public void checkAllMenus() {

        Logger.Log(LOG_FILE,"checkAllMenus", "Checking all elements are present in the Login Page Rishtey ",driver,true);

        assertElementPresent(registeredEMail,"registeredEMail");
        assertElementPresent(password,"password");
        assertElementPresent(submit,"submit");
        assertElementPresent(elmHomeMenu,"Home Menu");
        assertElementPresent(elmPROFILEMenu,"Profile Menu");
        assertElementPresent(elmRishteyMenu,"Rishtey Menu");
        assertElementPresent(elmCLAIMENTRYMenu,"Claim Entry Menu");
        assertElementPresent(elmMYCLAIMSMenu,"My Claims Menu");
        assertElementPresent(elmREWARDMenu,"Rewards Menu");
        assertElementPresent(elmPROFILEMenu,"Profile Menu");
        assertElementPresent(elmCONTACTMenu,"Contact Menu");
        assertElementPresent(elmSPECIALMenu,"Special Menu");
        assertElementPresent(elmCHNGPWDMenu,"Change Password Menu");
        assertElementPresent(elmLOGOUTMenu,"Logout Menu");


    }

    public void TypeUsername(String email) {

        Logger.Log(LOG_FILE,"TypeUsername","Inside the method which sends username to the email field",driver,true);

        waitforElement_to_Load(registeredEMail, "Email Field");
        SetText(registeredEMail, email,"Email field");
    }

    public void TypePassword(String Password) {

        Logger.Log(LOG_FILE,"TypePassword","Inside the method which sends password to the password field",driver,true);
        waitforElement_to_Load(password, "Password Field");
        SetText(password, Password, "Password field");
    }

    public void click_submit_button() {

        Logger.Log(LOG_FILE,"click_submit_button","Inside the method which Clicks Login button",driver,true);
        WaitforElementToLoadAndClick(submit,"Submit btn");
    }

    public void Logout() {

        WaitforElementToLoadAndClick(elmLOGOUTMenu, "Logout Menu");
    }

    public void Profile() {

        WaitforElementToLoadAndClick(elmPROFILEMenu, "Profile menu");
    }


    public void Login_without_password(String email) {

        Logger.Log(LOG_FILE,"","Inside the method Login without password",driver,true);
        TypeUsername(email);
        click_submit_button();
        waitForAlertAndAccept("Sorry - your credentials don't match with what we have in file");
    }

    public void Login_without_email(String password) {

        Logger.Log(LOG_FILE,"","Inside the method Login without email id",driver,true);
        TypePassword(password);
        click_submit_button();
        waitForAlertAndAccept("Sorry - your credentials don't match with what we have in file");
    }

    public void Login_with_both_Email_and_Password(String email, String Password) throws InterruptedException {

        Logger.Log(LOG_FILE,"","Inside the method Login with both email and  password",driver,true);
        TypeUsername(email);
        TypePassword(Password);
        click_submit_button();
        waitForAlertAndAccept("Login Successful. Now you will be redirected to home page ...");


    }
}