package com.annectos.webdriver.PageObjects.RishteyClaim;


import com.annectos.webdriver.Common.CommonMethods;
import com.annectos.webdriver.Helper.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import sun.rmi.runtime.Log;

import java.util.Iterator;
import java.util.List;

/**
 * Created by praveen on 7/8/2014.
 *
 */
public class Rishtey_My_Claims_PageObjects extends CommonMethods {

    @FindBy(css="div.pagetitle")
    public WebElement elm_MyClaim_Label_MY_CLAIMS;

    @FindBy(css="div > div:nth-child(2) > div > table > tbody > tr > td:nth-child(1)")
    public WebElement elm_MyClaim_Label_INVOICE_NUMBER;

    @FindBy(css="  div > div:nth-child(2) > div > table > tbody > tr > td:nth-child(2) > a")
    public WebElement elm_MyClaim_Label_INVOICE_DATE;

    @FindBy(css="div > div:nth-child(2) > div > table > tbody > tr > td:nth-child(3) > a")
    public WebElement elm_MyClaim_Label_INVOICE_TOTAL;

    @FindBy(css="div > div:nth-child(2) > div > table > tbody > tr > td:nth-child(4) > a")
    public WebElement elm_MyClaim_Label_STATUS;


    public Rishtey_My_Claims_PageObjects(WebDriver adriver) {
        super(adriver);
        driver = adriver;
        wait = new WebDriverWait(driver, timeOut);
        driver.manage().window().maximize();
    }

    public void Checkall_labels( ) {
        assertLabelText("MY CLAIMS", elm_MyClaim_Label_MY_CLAIMS);
        assertLabelText("INVOICE NUMBER", elm_MyClaim_Label_INVOICE_NUMBER);
        assertLabelText("INVOICE DATE", elm_MyClaim_Label_INVOICE_DATE);
        assertLabelText("INVOICE TOTAL", elm_MyClaim_Label_INVOICE_TOTAL);
        assertLabelText("STATUS", elm_MyClaim_Label_STATUS);


    }

    public void check_entered_claim_showsa(String text)
    {
        if(verifyTextPresent(text))
        {
            Logger.Log(LOG_FILE,"check_entered_claim_shows","Claim Entered by User "+text+" shows in My claims Page,",driver,true);
        }
        else
        {
            Logger.Log(LOG_FILE,"check_entered_claim_shows","Claim Entered by User "+text+ " doesn't shows in My claims Page,",driver,false);

        }
    }

    public void check_entered_claim_shows_and_The_Status(String InvoiceNumber,String Status)
    {

        WebElement table = driver.findElement(By.className("tablecolorborder"));
        List<WebElement> rows = table.findElements(By.tagName("tr"));
        Iterator<WebElement> i = rows.iterator();

        int ii=0;
        while(i.hasNext()){
            WebElement row=i.next();
            List<WebElement> columns= row.findElements(By.tagName("td"));
            Iterator<WebElement> j = columns.iterator();
            ii++;
            while(j.hasNext()){

                WebElement column = j.next();

                if(column.getText().equalsIgnoreCase(InvoiceNumber))
                {

                    System.out.print(column.getText());
                    System.out.println(ii);
                    String StatusOfTheClaim=driver.findElement(By.cssSelector("div.ng-scope > div > div:nth-child(2) > div > table > tbody > tr:nth-child("+ii+") > td:nth-child(4)")).getText();
                    Assert.assertEquals(StatusOfTheClaim,Status);
                    Logger.Log(LOG_FILE,"check_entered_claim_shows_and_The_Status","The Claim with Invoicenumber : "+InvoiceNumber+" Added is showing with status "+Status,driver,true);
                    break;
                }

            }
        }

    }



}
