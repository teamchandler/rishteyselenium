package com.annectos.webdriver.PageObjects.RishteyClaim;

import com.annectos.webdriver.Common.CommonMethods;
import com.annectos.webdriver.Helper.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Iterator;
import java.util.List;

/**
 * Created by praveen on 7/7/2014.
 */
public class Rishtey_Admin_Program_PageObjects extends CommonMethods {

    public Rishtey_Admin_Program_PageObjects(WebDriver adriver) {
        super(adriver);
        driver = adriver;
        wait = new WebDriverWait(driver, timeOut);
        driver.manage().window().maximize();
    }

    @FindBy(xpath = "(//input[@name='status'])[1]")
    public WebElement elm_ProgramAdmin_Radio_pending;

    @FindBy(xpath = "(//input[@name='status'])[2]")
    public WebElement elm_ProgramAdmin_Radio_VerandApproved;

    @FindBy(xpath = "(//input[@name='status'])[3]")
    public WebElement elm_ProgramAdmin_Radio_SVerRequired;

    @FindBy(xpath = "(//input[@name='status'])[4]")
    public WebElement elm_ProgramAdmin_Radio_Rejected;

    @FindBy(xpath = "(//input[@name='status'])[5]")
    public WebElement elm_ProgramAdmin_Radio_All;

    @FindBy(css="div.profileinfo.pull-left > div:nth-child(2) > p > label")
    public WebElement elm_ProgramAdmin_label_SIname;

    @FindBy(css="div.profileinfo.pull-left > div:nth-child(1) > div > p > label:nth-child(2)")
    public WebElement elm_ProgramAdmin_label_Pending;

    @FindBy(css="div.profileinfo.pull-left > div:nth-child(1) > div > p > label:nth-child(3)")
    public WebElement elm_ProgramAdmin_label_VerandApproved;

    @FindBy(css="div.profileinfo.pull-left > div:nth-child(1) > div > p > label:nth-child(4)")
    public WebElement elm_ProgramAdmin_label_SVR;

    @FindBy(css="div.profileinfo.pull-left > div:nth-child(1) > div > p > label:nth-child(5)")
    public WebElement elm_ProgramAdmin_label_Rejected;

    @FindBy(css="div.profileinfo.pull-left > div:nth-child(1) > div > p > label:nth-child(6)")
    public WebElement elm_ProgramAdmin_label_All;

    @FindBy(css="div.pagetitle")
    public WebElement elm_programAdmin_Label_ProgramManagement;

    @FindBy(css="span.heading")
    public WebElement elm_programAdmin_Label_INVOICEDETAILS;





  //
    public void CheckRadio_buttons() {


        VerifyElementPresent(elm_ProgramAdmin_Radio_pending, "Invoice number field");
        VerifyElementPresent(elm_ProgramAdmin_Radio_VerandApproved, "Invoice number field");
        VerifyElementPresent(elm_ProgramAdmin_Radio_SVerRequired, "Invoice number field") ;
        VerifyElementPresent(elm_ProgramAdmin_Radio_Rejected, "Invoice number field");
        VerifyElementPresent(elm_ProgramAdmin_Radio_All, "Invoice number field");
        VerifyElementPresent(elm_ProgramAdmin_label_SIname, "Invoice number field") ;
        VerifyElementPresent(elm_ProgramAdmin_label_Pending, "Invoice number field");
        VerifyElementPresent(elm_ProgramAdmin_label_VerandApproved, "Invoice number field");
        VerifyElementPresent(elm_ProgramAdmin_label_SVR, "Invoice number field");
        VerifyElementPresent(elm_ProgramAdmin_label_Rejected, "Invoice number field");
        VerifyElementPresent(elm_ProgramAdmin_label_All, "Invoice number field");


    }

    public void Checkall_labels( )
    {
        assertLabelText("Pending",elm_ProgramAdmin_label_Pending);
        assertLabelText("Verified & Approved",elm_ProgramAdmin_label_VerandApproved);
        assertLabelText("Schneider Verification Required",elm_ProgramAdmin_label_SVR);
        assertLabelText("Rejected",elm_ProgramAdmin_label_Rejected);
        assertLabelText("All",elm_ProgramAdmin_label_All);
        assertLabelText("PROGRAM MANAGEMENT", elm_programAdmin_Label_ProgramManagement);
        assertLabelText("INVOICE DETAILS", elm_programAdmin_Label_INVOICEDETAILS);
    }

    public void Select_Pending_radio()
    {
        WaitforElementToLoadAndClick(elm_ProgramAdmin_Radio_pending, "Submit btn");
    }

    public void Select_Verified_and_Approved_radio()
    {
        WaitforElementToLoadAndClick(elm_ProgramAdmin_Radio_VerandApproved, "Submit btn");

    }

    public void Select_Schneider_Verification_Required_radio()
    {
        WaitforElementToLoadAndClick(elm_ProgramAdmin_Radio_SVerRequired, "Submit btn");

    }

    public void Select_Rejected_radio()
    {
        WaitforElementToLoadAndClick(elm_ProgramAdmin_Radio_Rejected, "Submit btn");

    }

    public void Select_All_radio() throws InterruptedException {
        WaitforElementToLoadAndClick(elm_ProgramAdmin_Radio_All, "Submit btn");
        Thread.sleep(9000);

    }
    public void check_entered_claim_shows(String amount)
    {

       // program_management_table(amount);
        if(verifyTextPresent(amount))
        {
            Logger.Log(LOG_FILE, "check_entered_claim_shows", "Claim Entered by User " + amount + " shows in My claims Page,", driver, true);
        }
        else
        {
            Logger.Log(LOG_FILE,"check_entered_claim_shows","Claim Entered by User "+amount+ " doesn't shows in My claims Page,",driver,false);

        }
    }



    public void Click_Details_of_the_Specific_Invoice(String amount)
    {

        WebElement table = driver.findElement(By.className("tablecolorborder"));
        List<WebElement> rows = table.findElements(By.tagName("tr"));
        Iterator<WebElement> i = rows.iterator();

        int ii=0;
        while(i.hasNext()){
            WebElement row=i.next();
            List<WebElement> columns= row.findElements(By.tagName("td"));
            Iterator<WebElement> j = columns.iterator();
            ii++;
            while(j.hasNext()){

                WebElement column = j.next();
                System.out.println(column.getText()+"/n");
                if(column.getText().equalsIgnoreCase(amount))
                {

                    System.out.print(column.getText());
                    System.out.println(ii);
                    driver.findElement(By.cssSelector("div.ng-scope > div > div:nth-child(2) > div > div > div:nth-child(3) > div > table > tbody > tr:nth-child("+ii+") > td:nth-child(6) > button")).click();
                     break;
                }

            }
        }

    }

}
