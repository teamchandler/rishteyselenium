package com.annectos.webdriver.PageObjects.Promo;
import com.annectos.webdriver.Common.CommonMethods;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
/**
 * Created by praveen on 11/10/2014.
 */
public class SamplePromoPAgeObjects extends CommonMethods{

    @FindBy(xpath = "//button[text()='Toggle navigation'][1]")
    public WebElement toggleNavigation;

    @FindBy(xpath = "/html/body/div[2]/div/div[1]/div/div/div[1]/div[2]/select")
    public WebElement pleaseAddName1;

    @FindBy(css = "div:nth-child(2) > div.col-md-5.column > div:nth-child(2) > div:nth-child(1) > div > div > img")
    public WebElement Image1;

    @FindBy(css = "div:nth-child(2) > div.col-md-5.column > div:nth-child(2) > div:nth-child(2) > div > div > img")
    public WebElement Image2;

    @FindBy(css = "div:nth-child(2) > div.col-md-5.column > div:nth-child(2) > div:nth-child(3) > div > div > img")
    public WebElement Image3;

    @FindBy(css = "div:nth-child(2) > div.col-md-5.column > div:nth-child(4) > div:nth-child(1) > div > div > img")
    public WebElement Image4;

    @FindBy(css = "div:nth-child(2) > div.col-md-5.column > div:nth-child(4) > div:nth-child(2) > div > div > img")
    public WebElement Image5;

    @FindBy(xpath = "/html/body/div[2]/div/div[1]/div/div/div[2]/div[2]/div/form/div[1]/div[1]/input")
    public WebElement firstName;

    @FindBy(xpath = "/html/body/div[2]/div/div[1]/div/div/div[2]/div[2]/div/form/div[1]/div[2]/input")
    public WebElement lastName;

    @FindBy(id = "id2")
    public WebElement company;

    @FindBy(id = "id3")
    public WebElement designation;

    @FindBy(id = "id4")
    public WebElement emailAddress;

    @FindBy(id = "id5")
    public WebElement phone;

    @FindBy(id = "id6")
    public WebElement addressLine1;

    @FindBy(id = "id7")
    public WebElement addressLine2;

    @FindBy(id = "id8")
    public WebElement zipCode;

    @FindBy(xpath = "//button[text()='Claim your Tee']")
    public WebElement claimYourTee;

    public SamplePromoPAgeObjects(WebDriver adriver) {
        super(adriver);
        driver = adriver;
        wait = new WebDriverWait(driver, timeOut);
        driver.manage().window().maximize();
    }

    public void Goto_Promo()
    {
        driver.navigate().to(baseURL+"/samplepromo");
    }

    public void SetFirstname(String Fname)
    {
        waitforElement_to_Load(firstName, "First Name Field");
        SetText(firstName, Fname, "Email field");
    }

    public void Setlasttname(String Lname)
    {
        waitforElement_to_Load(lastName, "Last Name Field");
        SetText(lastName, Lname, "Email field");
    }

    public void SetCompanyname(String companyname)
    {
        waitforElement_to_Load(company, "Company Field");
        SetText(company, companyname, "Email field");
    }

    public void Setdesignation(String designations)
    {
        waitforElement_to_Load(designation, "Designation Field");
        SetText(designation, designations, "Email field");
    }

    public void SetEmail(String email)
    {
        waitforElement_to_Load(emailAddress, "Email Field");
        SetText(emailAddress, email, "Email field");
    }

    public void SetPhone(String phoneno)
    {
        waitforElement_to_Load(phone, "Phone no. Field");
        SetText(phone, phoneno, "Email field");
    }

    public void SetaddressLine1(String addressLine_1)
    {
        waitforElement_to_Load(addressLine1, "Address Field");
        SetText(addressLine1, addressLine_1, "Email field");
    }

    public void SetaddressLine2(String addressLine_2)
    {
        waitforElement_to_Load(addressLine2, "Address 2 Field");
        SetText(addressLine2, addressLine_2, "Email field");
    }

    public void SetZipCode(String Zipcode)
    {
        waitforElement_to_Load(zipCode, "ZIP Field");
        SetText(zipCode, Zipcode, "Email field");
    }

    public void Click_image1()
    {

        WaitforElementToLoadAndClick(Image1, "Submit btn");
    }
    public void Click_image2()
    {

        WaitforElementToLoadAndClick(Image2, "Submit btn");
    }
    public void Click_image3()
    {

        WaitforElementToLoadAndClick(Image3, "Submit btn");
    }
    public void Click_image4()
    {

        WaitforElementToLoadAndClick(Image4, "Submit btn");
    }
    public void Click_image5()
    {

        WaitforElementToLoadAndClick(Image5, "Submit btn");
    }

    public void Fill_Claim_tee_Details_without_Size(String Fname, String Lname, String desig, String CompanyNmae,
                                       String email, String Phone, String addrL1, String addrL2, String zipcode)
    {
        SetFirstname(Fname);
        Setlasttname(Lname);
        SetCompanyname(CompanyNmae);
        Setdesignation(desig) ;
        SetEmail(email);
        SetPhone(Phone) ;
        SetaddressLine1(addrL1);
        SetaddressLine2(addrL2);
        SetZipCode(zipcode);
        Click_Claim_Button();
        waitForAlertAndAccept("Please select the product size!") ;

    }



    public void Click_Claim_Button()
    {
        WaitforElementToLoadAndClick(claimYourTee, "Submit btn");
    }


}
